package GUI;

import GUIEngine.AtmEngine;
import GUIEngine.Client;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;


public class AtmGUI {
    
        //Constants
        final private static int START = 1;
        final private static int GETMONEY = 2;
        final private static int GETBALANCE = 3;
        final private static int TRANSFER = 4;
        
        private static int state = START;

	private JFrame frame;
	private JButton btn1;
	private JButton btn2;
	private JButton btn3;
	private JButton btn4;
	private JButton btn5;
	private JButton btn6;
	private JButton btn7;
	private JButton btn8;
	private JButton btn9;
	private JButton btn0;
	private JTextField textField;
	private JButton getMoneyBtn;
	private JButton balanceBtn;
	private JButton remittanceBtn;
	private JButton acceptBtn;
	private JButton cancelBtn;
	private JButton closeBtn;
        private JButton colonBtn;
        
        public JFrame getFrame(){
            return frame;
        }
        
	
	public String getDisplayText(){
		return textField.getText();
	}
	
	public void setDisplayText(String text){
		textField.setText(text);
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AtmGUI window = new AtmGUI();
					window.frame.pack();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AtmGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
            frame = new JFrame();
            frame.setResizable(false);
            frame.setTitle("Банкомат");
            frame.setBounds(100, 100, 397, 171);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            GridBagLayout gridBagLayout = new GridBagLayout();
            gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
            gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
            gridBagLayout.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
            gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
            frame.getContentPane().setLayout(gridBagLayout);

            KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
            manager.addKeyEventDispatcher(new AtmEngine(this));

            textField = new JTextField();
            textField.setEditable(false);
            textField.setText("");
            textField.setHorizontalAlignment(SwingConstants.CENTER);
            textField.setToolTipText("Введите 'номер карточки':'ПИН-код'");
            GridBagConstraints gbc_textField = new GridBagConstraints();
            gbc_textField.gridwidth = 5;
            gbc_textField.insets = new Insets(0, 0, 5, 0);
            gbc_textField.fill = GridBagConstraints.HORIZONTAL;
            gbc_textField.gridx = 0;
            gbc_textField.gridy = 0;
            frame.getContentPane().add(textField, gbc_textField);
            textField.setColumns(4);		

            getMoneyBtn = new JButton("Снять деньги");
            getMoneyBtn.setEnabled(false);
            GridBagConstraints gbc_getMoneyBtn = new GridBagConstraints();
            gbc_getMoneyBtn.fill = GridBagConstraints.BOTH;
            gbc_getMoneyBtn.insets = new Insets(0, 0, 5, 5);
            gbc_getMoneyBtn.gridx = 0;
            gbc_getMoneyBtn.gridy = 1;
            frame.getContentPane().add(getMoneyBtn, gbc_getMoneyBtn);

            btn1 = new JButton("1");
            btn1.addActionListener(new AtmEngine(this));
            GridBagConstraints gbc_btn1 = new GridBagConstraints();
            gbc_btn1.insets = new Insets(0, 0, 5, 5);
            gbc_btn1.gridx = 1;
            gbc_btn1.gridy = 1;
            frame.getContentPane().add(btn1, gbc_btn1);

            btn2 = new JButton("2");
            btn2.addActionListener(new AtmEngine(this));
            GridBagConstraints gbc_btn2 = new GridBagConstraints();
            gbc_btn2.anchor = GridBagConstraints.SOUTH;
            gbc_btn2.insets = new Insets(0, 0, 5, 5);
            gbc_btn2.gridx = 2;
            gbc_btn2.gridy = 1;
            frame.getContentPane().add(btn2, gbc_btn2);

            btn3 = new JButton("3");
            btn3.addActionListener(new AtmEngine(this));
            GridBagConstraints gbc_btn3 = new GridBagConstraints(); 
            gbc_btn3.insets = new Insets(0, 0, 5, 5);
            gbc_btn3.gridx = 3;
            gbc_btn3.gridy = 1;
            frame.getContentPane().add(btn3, gbc_btn3);

            acceptBtn = new JButton("Принять");
            acceptBtn.addActionListener(new AtmEngine(this){
                public void actionPerformed(ActionEvent e){
                    String dataString = textField.getText();
                    if (state == START)
                        if ((dataString.indexOf(':') == -1) ||
                            (dataString.indexOf(':') == 0)  ||
                            (dataString.indexOf(':') == textField.getText().length()))
                            JOptionPane.showMessageDialog(frame, " Неверный формат!\n Введите 'номер карточки':'ПИН-код'");
                        else {
                        
                    }
                   
                                                         
                    
                    textField.setText(""); 
                }
                
            });
            GridBagConstraints gbc_acceptBtn = new GridBagConstraints();
            gbc_acceptBtn.ipadx = 40;
            gbc_acceptBtn.fill = GridBagConstraints.BOTH;
            gbc_acceptBtn.insets = new Insets(0, 0, 5, 0);
            gbc_acceptBtn.gridx = 4;
            gbc_acceptBtn.gridy = 1;
            frame.getContentPane().add(acceptBtn, gbc_acceptBtn);

            balanceBtn = new JButton("Посмотреть баланс");
            balanceBtn.setEnabled(false);
            GridBagConstraints gbc_balanceBtn = new GridBagConstraints();
            gbc_balanceBtn.fill = GridBagConstraints.BOTH;
            gbc_balanceBtn.anchor = GridBagConstraints.SOUTHWEST;
            gbc_balanceBtn.insets = new Insets(0, 0, 5, 5);
            gbc_balanceBtn.gridx = 0;
            gbc_balanceBtn.gridy = 2;
            frame.getContentPane().add(balanceBtn, gbc_balanceBtn);

            btn4 = new JButton("4");
            btn4.addActionListener(new AtmEngine(this));
            GridBagConstraints gbc_btn4 = new GridBagConstraints();
            gbc_btn4.insets = new Insets(0, 0, 5, 5);
            gbc_btn4.gridx = 1;
            gbc_btn4.gridy = 2;
            frame.getContentPane().add(btn4, gbc_btn4);

            btn5 = new JButton("5");
            btn5.addActionListener(new AtmEngine(this));
            GridBagConstraints gbc_btn5 = new GridBagConstraints();
            gbc_btn5.insets = new Insets(0, 0, 5, 5);
            gbc_btn5.gridx = 2;
            gbc_btn5.gridy = 2;
            frame.getContentPane().add(btn5, gbc_btn5);

            btn6 = new JButton("6");
            btn6.addActionListener(new AtmEngine(this));
            GridBagConstraints gbc_btn6 = new GridBagConstraints();
            gbc_btn6.insets = new Insets(0, 0, 5, 5);
            gbc_btn6.gridx = 3;
            gbc_btn6.gridy = 2;
            frame.getContentPane().add(btn6, gbc_btn6);

            cancelBtn = new JButton("Очистить");
            cancelBtn.addActionListener(new AtmEngine(this){
                public void actionPerformed(ActionEvent e){
                    textField.setText("");
                }
            });
            GridBagConstraints gbc_cancelBtn = new GridBagConstraints();
            gbc_cancelBtn.ipadx = 40;
            gbc_cancelBtn.fill = GridBagConstraints.BOTH;
            gbc_cancelBtn.insets = new Insets(0, 0, 5, 0);
            gbc_cancelBtn.gridx = 4;
            gbc_cancelBtn.gridy = 2;
            frame.getContentPane().add(cancelBtn, gbc_cancelBtn);

            remittanceBtn = new JButton("Перевести деньги");
            remittanceBtn.setEnabled(false);
            GridBagConstraints gbc_remittanceBtn = new GridBagConstraints();
            gbc_remittanceBtn.fill = GridBagConstraints.BOTH;
            gbc_remittanceBtn.insets = new Insets(0, 0, 5, 5);
            gbc_remittanceBtn.gridx = 0;
            gbc_remittanceBtn.gridy = 3;
            frame.getContentPane().add(remittanceBtn, gbc_remittanceBtn);

            btn7 = new JButton("7");
            btn7.addActionListener(new AtmEngine(this));
            GridBagConstraints gbc_btn7 = new GridBagConstraints();
            gbc_btn7.insets = new Insets(0, 0, 5, 5);
            gbc_btn7.gridx = 1;
            gbc_btn7.gridy = 3;
            frame.getContentPane().add(btn7, gbc_btn7);

            btn8 = new JButton("8");
            btn8.addActionListener(new AtmEngine(this));
            GridBagConstraints gbc_btn8 = new GridBagConstraints();
            gbc_btn8.insets = new Insets(0, 0, 5, 5);
            gbc_btn8.gridx = 2;
            gbc_btn8.gridy = 3;
            frame.getContentPane().add(btn8, gbc_btn8);

            btn9 = new JButton("9");
            btn9.addActionListener(new AtmEngine(this));
            GridBagConstraints gbc_btn9 = new GridBagConstraints();
            gbc_btn9.insets = new Insets(0, 0, 5, 5);
            gbc_btn9.gridx = 3;
            gbc_btn9.gridy = 3;
            frame.getContentPane().add(btn9, gbc_btn9);

            closeBtn = new JButton("Выход");
            GridBagConstraints gbc_closeBtn = new GridBagConstraints();
            gbc_closeBtn.ipadx = 40;
            gbc_closeBtn.fill = GridBagConstraints.BOTH;
            gbc_closeBtn.insets = new Insets(0, 0, 5, 0);
            gbc_closeBtn.gridx = 4;
            gbc_closeBtn.gridy = 3;
            frame.getContentPane().add(closeBtn, gbc_closeBtn);

            btn0 = new JButton("0");
            btn0.addActionListener(new AtmEngine(this));
            GridBagConstraints gbc_btn0 = new GridBagConstraints();
            gbc_btn0.fill = GridBagConstraints.HORIZONTAL;
            gbc_btn0.gridwidth = 2;
            gbc_btn0.insets = new Insets(0, 0, 0, 5);
            gbc_btn0.gridx = 1;
            gbc_btn0.gridy = 4;
            frame.getContentPane().add(btn0, gbc_btn0);
            
            colonBtn = new JButton(":");
            colonBtn.addActionListener(new AtmEngine(this));
            GridBagConstraints gbc_colonBtn = new GridBagConstraints();
            gbc_colonBtn.fill = GridBagConstraints.HORIZONTAL;
            gbc_colonBtn.gridwidth = 1;
            gbc_colonBtn.insets = new Insets(0, 0, 0, 5);
            gbc_colonBtn.gridx = 3;
            gbc_colonBtn.gridy = 4;
            frame.getContentPane().add(colonBtn, gbc_colonBtn);
	}

}
