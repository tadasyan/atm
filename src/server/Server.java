/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alexey
 */
public class Server {
    
    private static String getMD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);
            // Now we need to zero pad it if you actually want the full 32 chars.
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
    
    public static void main(String[] args) {
        
        System.out.println("Server start");
        String url = "jdbc:postgresql://localhost/ATMDB";
        String user = "postgres";
        String passwordCon = "weueqkj[p";
        int portNumber = 3000;
        while (true){
            boolean f = false;
            try ( 
                ServerSocket serverSocket = new ServerSocket(portNumber);
                Socket clientSocket = serverSocket.accept();
                PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(clientSocket.getInputStream()));
                Connection con = DriverManager.getConnection(url, user, passwordCon);
                PreparedStatement pst_credit = con.prepareStatement(
                        "SELECT credit, limit, name "
                                + "FROM creditAccount, owners "
                                + "WHERE password=? AND acc_number=? AND id=id_owner");
                PreparedStatement pst_master = con.prepareStatement(
                        "SELECT balance, name "
                                + "FROM masterAccount, owners "
                                + "WHERE password=? AND acc_number=? AND id=id_owner");
                //ResultSet rs_credit = pst_credit.executeQuery();
                //ResultSet rs_master = pst_master.executeQuery();
            ) {
                String number = in.readLine().substring(0, in.readLine().indexOf(':'));
                String password = in.readLine().substring(in.readLine().indexOf(':'));
                pst_credit.setString(1, getMD5(password));
                pst_credit.setString(2, number);
                ResultSet rs_credit = pst_credit.executeQuery();
                pst_master.setString(1, getMD5(password));
                pst_master.setString(2, number);
                ResultSet rs_master = pst_master.executeQuery();
                while (rs_credit.next()){
                    out.println(rs_credit.getString("name"));
                    out.println(rs_credit.getString("credit"));
                    out.println(rs_credit.getString("limit"));
                }
                while (rs_master.next()){
                    out.println(rs_credit.getString("name"));
                    out.println(rs_credit.getString("balance"));
                }
                if (f) 
                    out.println("true");
                else 
                    out.println("false");
            } catch (SQLException e){
                System.out.println("SQLException");
                System.out.println(e.getMessage());
            } catch (IOException e) {
                System.out.println("Exception caught when trying to listen on port "
                    + portNumber + " or listening for a connection");
                System.out.println(e.getMessage());
            }
        
        }
    }
}
