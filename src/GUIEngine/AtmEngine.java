package GUIEngine;

import java.awt.KeyEventDispatcher;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JButton;

import GUI.AtmGUI;
import javax.swing.JOptionPane;

public class AtmEngine implements ActionListener, KeyEventDispatcher  {

	private AtmGUI parent;
	
	public AtmEngine (AtmGUI parent){
		this.parent = parent;
	}
        
        public static void showMessage(){
            JOptionPane.showMessageDialog(parent.getFrame(), " Неверный формат!\n Введите 'номер карточки':'ПИН-код'");
        }
        
        public String getText(){
            return parent.getDisplayText();
        }
	
        @Override
	public void actionPerformed(ActionEvent e){
			JButton clickedBtn = (JButton) e.getSource();
			/*if (parent.getDisplayText().equals("Введите номер карточки")) 
                            parent.setDisplayText("");*/
			parent.setDisplayText(parent.getDisplayText() + clickedBtn.getText());
	}
	
        @Override
	public boolean dispatchKeyEvent (KeyEvent e){
		if (e.getID() == KeyEvent.KEY_PRESSED){
			char key = e.getKeyChar();
			/*if (parent.getDisplayText().equals("Введите номер карточки")) 
                            parent.setDisplayText("");*/
			switch (key) {
				case '1': 
					parent.setDisplayText(parent.getDisplayText() + 1);
					break;
				case '2': 
					parent.setDisplayText(parent.getDisplayText() + 2);
					break;
				case '3': 
					parent.setDisplayText(parent.getDisplayText() + 3);
					break;
				case '4': 
					parent.setDisplayText(parent.getDisplayText() + 4);
					break;
				case '5': 
					parent.setDisplayText(parent.getDisplayText() + 5);
					break;
				case '6': 
					parent.setDisplayText(parent.getDisplayText() + 6);
					break;
				case '7': 
					parent.setDisplayText(parent.getDisplayText() + 7);
					break;
				case '8': 
					parent.setDisplayText(parent.getDisplayText() + 8);
					break;
				case '9': 
					parent.setDisplayText(parent.getDisplayText() + 9);
					break;
				case '0': 
					parent.setDisplayText(parent.getDisplayText() + 0);
					break;
                                case ':':
                                        parent.setDisplayText(parent.getDisplayText() + ":");
			}
		}
		return false;
	}
}
