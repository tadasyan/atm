/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package GUIEngine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * @author Alexey
 */
public class Client {
    
    /*private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;*/
    private String infoString;
    private final String HOSTNAME = "localhost";
    private final int PORT = 3000;
    
    public Client (String info){
        this.infoString = info;            
    }
    
    public BufferedReader checkPassword(){
        //boolean passFlag = false;
        try (
                Socket clientSocket = new Socket(HOSTNAME, PORT);
                PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                ){
            out.println(infoString);
            //passFlag = "true".equals(in.readLine());
            
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + HOSTNAME);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " +
                HOSTNAME);
            System.exit(1);
        }
        //return passFlag;
    }
}
